ring-clojure (1.8.2-3) unstable; urgency=medium

  * d/rules: fix double build issue (Closes: #1046176, #1049524)
  * d/patches: mark lein local patches not-needed upstream

 -- Jérôme Charaoui <jerome@riseup.net>  Thu, 15 Feb 2024 21:22:48 -0500

ring-clojure (1.8.2-2) unstable; urgency=medium

  * prevent publishing bogus jetty version in pom deps
  * d/control:
    + add myself to Uploaders
    + depend on clojure package providing 1.x
    + use headless jdk in build-deps
    - drop obsolete test-only build-deps
  * add built artifacts to maven-repo on the fly
  * use --java-lib to get rid of d/*.jlibs files

 -- Jérôme Charaoui <jerome@riseup.net>  Wed, 08 Feb 2023 11:29:30 -0500

ring-clojure (1.8.2-1) unstable; urgency=medium

  * Team upload.

  [ Louis-Philippe Véronneau ]
  * d/control: update the VCS links to point to the right repository.

  [ Jérôme Charaoui ]
  * New upstream version 1.8.2
  * d/control:
    + fix Maintainer field, should be clojure-team
    + clj-time optional for build
    + bump Standards-Version, no changes needed
  * d/patches: refresh patches for new upstream version
  * d/patches: add patches to fix 2 test cases
  * d/rules: make subproject loops more robust
  * d/rules: honor nocheck build option
  * d/tests: run autopkgtests with bultitude
  * add d/gbp.conf with clojure-team defaults

 -- Jérôme Charaoui <jerome@riseup.net>  Mon, 30 Jan 2023 15:41:54 -0500

ring-clojure (1.6.2-4) unstable; urgency=medium

  * Team upload.
  * Rebuild using leiningen.
  * d/watch: update to v4 and use git mode.
  * d/tests: add autopkgtests.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Tue, 22 Dec 2020 13:50:37 -0500

ring-clojure (1.6.2-3) unstable; urgency=medium

  * Team upload.
  * d/*.poms: install jars in maven-repo. (Closes: #977571)
  * d/control: use dh-compat and update to dh13.
  * d/control: Standards-Version update to 4.5.1. Add Rules-Requires-Root.

 -- Louis-Philippe Véronneau <pollo@debian.org>  Wed, 16 Dec 2020 18:24:40 -0500

ring-clojure (1.6.2-2) unstable; urgency=medium

  [ Cyril Brulebois ]
  * Sort filenames for tests found using find, to ensure reproducible
    lists of test files, as order matters for successful test runs. This
    fixes the intermittent FTBFS due to test failures (Closes: #918437).
  * Update Vcs-{Browser,Git} to point to salsa (alioth's replacement).

  [ Apollon Oikonomopoulos ]
  * Work around jh_classpath not being able to process jars in multiple
    packages
  * Bump Standards-Version to 4.3.0; no changes needed
  * d/copyright: bump debian/ years
  * Bump dh compat to 11; no changes needed

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Fri, 08 Feb 2019 22:32:59 +0200

ring-clojure (1.6.2-1) unstable; urgency=medium

  * Initial release (Closes: #855749)

 -- Apollon Oikonomopoulos <apoikos@debian.org>  Fri, 04 Aug 2017 19:16:23 -0400
